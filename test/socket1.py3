import socket

MYSOCK = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

MYSOCK.connect(('data.pr4e.org', 80))

CMD = 'GET http://data.pr4e.org/romeo.txt HTTP/1.0\n\n'.encode()

MYSOCK.send(CMD)

while True:
    DATA = MYSOCK.recv(512)
    if len(DATA) < 1:
        break
    print(DATA.decode())

MYSOCK.close()
