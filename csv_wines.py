'''
llegeix csv a wine-reviews i crea un diccionari amb les varietats,
desant el nom de varietat i la quantitat de vegades que surt
'''

import csv

with open('wine-reviews/winemag-data-130k-v2.csv', 'r', encoding="utf-8") as csvfile:
    reader = csv.reader(csvfile, delimiter=",", quotechar='"')
    count=0
    
    #print next(reader)
    #['id', 'country', 'description', 'designation', 'points', 
    # 'price', 'province', 'region_1', 'region_2', 'taster_name', 
    # 'taster_twitter_handle', 'title', 'variety', 'winery']
    
    varietats = {}
    for row in reader:
        if row[6]=="Catalonia":
            #print row[6], sep, row[7], sep, row[3], sep, row[12]
            if (row[12]=="Xarel-lo"):
                row[12]="Xarel·lo"
            vars = row[12].split('-')
            for var in vars:
                varietats[var] = varietats.get(var,0)+1
    
    print (varietats)
       

