'''
llegeix csv a wine-reviews i el desa a la bdd

'''

import csv

import pymysql.cursors

connection = pymysql.connect(host='192.168.10.10',
                             user='homestead',
                             password='secret',
                             db='wines',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

csv_file_path = 'wine-reviews/winemag-data-130k-v2.csv'

# Connect to the database

try:

    with open(csv_file_path, 'r', encoding="utf-8") as csvfile, connection.cursor() as cursor:

        reader = csv.reader(csvfile, delimiter=",", quotechar='"')

        for row in reader:
            if row[6]=="Catalonia":
                sql = "INSERT INTO `wines` (`id`, `country`, `description`, `designation`, `points`," \
                "`price`, `province`, `region1`, `region2`, `taster_name`," \
                "`taster_twitter`, `title`, `variety`, `winery`) VALUES "\
                "(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                cursor.execute(sql, (row[0],row[1],row[2],row[3],row[4],
                    row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12],row[13]))

                connection.commit()
                print (row[3])

finally:
    connection.close()


